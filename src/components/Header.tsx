/* eslint-disable jsx-a11y/anchor-is-valid */
import logo from '../logo.svg';
import {FaHamburger} from 'react-icons/fa';
const Header = () => {
    return (
        <header>
            <nav className="flex justify-between items-center relative px-4 py-9 z-10">
                <img src={logo} alt="Logo" className="w-24"/>
                <FaHamburger className="text-blue-400 w-8 h-8 mr-5"/>
                <ul className="space-y-8 absolute top-40 left-1/2 transform -translate-x-1/2 z-0 text-center">
                    <li><a href="#" className="uppercase font-bold">About</a></li>
                    <li><a href="#" className="uppercase font-bold">Menu</a></li>
                    <li><a href="#" className="uppercase font-bold">How It Works</a></li>
                    <li><a href="#" className="uppercase font-bold">Blog</a></li> 
                    <li><a href="#" className="uppercase font-bold">Be a Jagoan</a></li>
                </ul>
            </nav>
        </header>
    )
}

export default Header;